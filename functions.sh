#!----------------------------------------------------------------------------
#!
#! functions.sh 
#!
#! various functins for atlas-cvmfsexec
#!
#! Usage: 
#!     . functions.sh  
#!
#! History:
#!   12Jan23: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

acx_fn_installHelp()
{

    \cat <<EOF

    Usage: $acx_progname [options]

    Install or update cvmfsexec for ATLAS

    Options (to override defaults) are:
     -h  --help               Print this help message
     -c  --configFile=STRING  file to include in cvmfs/default.local
     	 		        (will append and so will overwrite defaults)
     -f  --force	      Force an installation / update
     	 		       User should unmount any processes first
     -p  --prefix=STRING      Where to install (default \$PWD)
     -s  --skipGit            Will skip any git for atlas-cvmfsexec after
                                the first time
     -v  --version=STRING     Version of atlas-cvmfsexec (default $acx_version)
     -z  --zipFile	      Use a zipfile instead of git clone for cvmfsexec
EOF
    
    return 0
}


acx_updateConfig()
{

    if [[ "$acx_forceInstall" != "YES" ]] && [[ -e $acx_installDir/dist/etc/cvmfs/acx_configDone ]]; then
	acx_fn_printMsg $acx_time0 "skipping config update as it was done ..."
	return 0
    else
	\rm -f $acx_installDir/dist/etc/cvmfs/acx_configDone
    fi
	 
    local acx_tmpVal=`pwd`
    cd $acx_installDir/dist/etc/cvmfs
    local acx_tmpAr=( `\cat $acx_configFile | \grep -e "^[[:alnum:]]" | \cut -f 1 -d "="` )
    local acx_item
    local acx_sedStr=""
    for acx_item in ${acx_tmpAr[@]}; do
	acx_sedStr="$acx_sedStr -e 's|^$acx_item=|\#$acx_item=|g'"
    done    
    
    if [ ! -e ./default.local.original ]; then
	\cp ./default.local ./default.local.original
	chmod --reference ./default.local ./default.local.original 
    fi
    if [ "$acx_sedStr" != "" ]; then
	eval \sed $acx_sedStr ./default.local.original > ./default.local.new
	chmod --reference ./default.local.original ./default.local.new
    fi
    \cat $acx_configFile >> ./default.local.new
    \mv ./default.local.new ./default.local

    touch $acx_installDir/dist/etc/cvmfs/acx_configDone

    cd $acx_tmpVal
    
    return 0
}


acx_fn_install() 
{

    let acx_time0=`date +%s`
    . $ACX_BASE/common.sh

    acx_fn_printMsg $acx_time0 "Start atlas-cvmfs installation ..."    
    
    acx_fn_getEnv

    local acx_shortOpts="h,p:,v:,f,c:,s,z"
    local acx_longOpts="help,prefix:,version:,force,configFile:,skipGit,zipFile"
    local acx_opts=$(getopt -o $acx_shortOpts --long $acx_longOpts -n "$acx_progname" -- "$@")
    if [ $? -ne 0 ]; then
	\echo "'$acx_progname --help' for more information" 1>&2
	return 1
    fi
    eval set -- "$acx_opts"

    local acx_prefix="`pwd`/cvmfsexec"
    local acx_version=""
    local acx_forceInstall="NO"
    local acx_configFile=""
    local acx_skipGit="NO"
    local acx_zipFile="NO"
    
    while [ $# -gt 0 ]; do
	: debug: $1
	case $1 in
            -h|--help)
		acx_fn_installHelp
		return 0
		;;
	    -c|--configFile)
		acx_configFile=$2
		shift 2
		if [ ! -e $acx_configFile ]; then
		    acx_fn_printMsg $acx_time0 "Error: config file $acx_configFile is missing.  Please specify complete path to file."
		    return 1
		else
		    acx_configFile=`readlink -f $acx_configFile`
		fi
		;;
	    -f|--force)
		acx_forceInstall="YES"
		shift
		;;
	    -p|--prefix)
		acx_prefix=$2
		shift 2
		;;
	    -v|--version)
		acx_version=$2
		shift 2
		;;
	    -s|--skipGit)
		acx_skipGit="YES"
		shift
		;;
	    -z|--zipFile)
		acx_zipFile="YES"
		shift
		;;
	    --)
		shift
		break
		;;
            *)
		\echo "Internal Error: option processing error: $1" 1>&2
		return 1
		;;
	esac
    done

    cd $ACX_BASE
    if [[ "$acx_skipGit" = "YES" ]] && [[ -e $ACX_BASE/skipGitUpdate ]] ; then
	acx_fn_printMsg $acx_time0 "Skipping update of atlas-cvmfsexec since it was done once"
    else
	git fetch origin
	if [ $? -ne 0 ]; then
	    return 64
	fi
	git reset --hard origin/master
	if [ $? -ne 0 ]; then
	    return 64
	fi
	if [ "$acx_version" = "" ]; then
	    . $ACX_BASE/latestVersion
	fi
	acx_fn_printMsg $acx_time0 "Update atlas-cvmfsexec to $acx_version ..."
	git checkout tags/$acx_version
	if [ $? -ne 0 ]; then
	    return 64
	fi
	if [ "$acx_skipGit" = "YES" ]; then
	    touch $ACX_BASE/skipGitUpdate
	fi
    fi
    
    \echo $acx_prefix | \grep -e "cvmfsexec$" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	acx_prefix="$acx_prefix/cvmfsexec"
    fi
    
    local acx_installDir="$acx_prefix/$acx_arch/centos$acx_os"
    \mkdir -p $acx_installDir
    if [ $? -ne 0 ]; then
	return 1
    fi

    local acx_installIt="YES"
    if [ -d $acx_installDir/dist/etc/cvmfs ]; then
	if [ "$acx_forceInstall" = "YES" ]; then
	    \rm -rf $acx_installDir/dist
	    acx_installIt="YES"
	else
	    \echo "Skipping installation of cvmfsexec since it exists in $acx_installDir"
	    \echo "  to force installation, unmount and kill processes and use the -f/--force option"
	    local acx_tmp=`\find  $acx_installDir/dist/cvmfs/ -maxdepth 1 -mindepth 1`
	    if [ "$acx_tmp" != "" ]; then
	    	\echo "These repos may still be mounted.  Unmount them first !"
		\echo $acx_tmp
	    fi
	    acx_installIt="NO"
	fi
    fi
    
    if [ "$acx_installIt" = "YES" ]; then
	cd $ACX_BASE
	. $ACX_BASE/cvmfsexecVersion.sh 
	if [[ -d $acx_installDir/.git ]] && [[ "$acx_zipFile" = "YES" ]] ; then
	    acx_fn_printMsg $acx_time0 "cvmfsexec was installed by git. Continue to use it instead of zip file  ..."
	    acx_zipFile="NO" 
	elif [[ -e $acx_installDir/acx_cvmfsIsInstalledZip ]] && [[ "$acx_zipFile" = "NO" ]] ; then
	    acx_fn_printMsg $acx_time0 "cvmfsexec was installed by zip. Continue to use it instead of git  ..."
	    acx_zipFile="YES" 
	fi
	\rm -rf $acx_installDir/dist

	if [ "$acx_zipFile" = "YES" ]; then
	    if [ ! -e $ACX_BASE/$cvmfsexecVersion.zip ]; then
		acx_fn_printMsg $acx_time0 "Fetch https://atlas-tier3-sw.web.cern.ch/repo/cvmfsexec/$cvmfsexecVersion.zip  ..."
		wget https://atlas-tier3-sw.web.cern.ch/repo/cvmfsexec/$cvmfsexecVersion.zip
		if [ $? -ne 0 ]; then
		    acx_fn_printMsg $acx_time0 "Fetch https://github.com/cvmfs/cvmfsexec/archive/refs/tags/$cvmfsexecVersion.zip ..."
		    wget https://github.com/cvmfs/cvmfsexec/archive/refs/tags/$cvmfsexecVersion.zip
		    if [ $? -ne 0 ]; then
			acx_fn_printMsg $acx_time0 "Error: unable to download cvmfsexec file"
			return 1
		    fi
		fi
	    fi
	    \rm -rf $acx_installDir/tmp
	    \mkdir -p $acx_installDir/tmp
	    \unzip -d $acx_installDir/tmp $ACX_BASE/$cvmfsexecVersion.zip
	    if [ $? -ne 0 ]; then
		return 1
	    fi
	    local acx_tmpDir=`\find $acx_installDir/tmp -name cvmfsexec -type f`
	    if [ $? -ne 0 ]; then
		return 1
	    fi
	    acx_tmpDir=`\dirname $acx_tmpDir`
	    cd $acx_tmpDir
	    \mv * $acx_installDir/
	    \mv .* $acx_installDir/
	    cd $acx_installDir
	    \rm -rf $acx_tmpDir
	    touch $acx_installDir/acx_cvmfsIsInstalledZip
	    
	else	    
	    if [[ ! -d $acx_installDir/.git ]] && [[ ! -e $acx_installDir/acxInstalledVersion ]]; then
		acx_fn_printMsg $acx_time0 "Clone cvmfsexec to $acx_installDir ..."
		git clone https://github.com/cvmfs/cvmfsexec.git $acx_installDir
		if [ $? -ne 0 ]; then
		    return 1
		fi
	    fi

	    . $ACX_BASE/cvmfsexecVersion.sh 
	    acx_fn_printMsg $acx_time0 "Update cvmfsexec version to $cvmfsexecVersion ..."
	    cd $acx_installDir
	    git checkout tags/$cvmfsexecVersion
	    if [ $? -ne 0 ]; then
		return 1
	    fi

	fi
	
	acx_fn_printMsg $acx_time0 "Running ./makedist osg ..."
	./makedist osg
	if [ $? -ne 0 ]; then
            return 1
	fi
    fi

    if [ "$acx_configFile" != "" ]; then
	acx_fn_printMsg $acx_time0 "updating config file ..."
	acx_updateConfig
    fi
    
    acx_fn_printMsg $acx_time0 "creatng setup scripts ..."
    acx_create_setupScript

    \echo "$cvmfsexecVersion" > $acx_installDir/acxInstalledVersion.tmp
    \mv $acx_installDir/acxInstalledVersion.tmp $acx_installDir/acxInstalledVersion

    acx_fn_printMsg $acx_time0 "End atlas-cvmfsexec installation."
    
    return 0
}


acx_create_setupScript()
{
    local let acx_rc=0

    if [[ "$acx_forceInstall" = "YES" ]] || [[ ! -e $acx_prefix/mountpoints.sh ]]; then
	\cat <<EOF >> $acx_prefix/mountpoints.sh.tmp
mountPointAr=(
    "config-osg.opensciencegrid.org"
    "atlas.cern.ch"
    "atlas-condb.cern.ch"
    "atlas-nightlies.cern.ch"
    "sft.cern.ch"
    "sft-nightlies.cern.ch"
    "unpacked.cern.ch"
)
EOF
	let acx_rc=$?
	if [ $acx_rc -eq 0 ]; then
	    \mv $acx_prefix/mountpoints.sh.tmp $acx_prefix/mountpoints.sh
	fi
    fi

    if [[ "$acx_forceInstall" = "YES" ]] || [[ ! -e $acx_prefix/force-cleanup.sh ]]; then
	\cat <<EOF >> $acx_prefix/force-cleanup.sh.tmp    
let acx_time0=\`date +%s\`

. $ACX_BASE/common.sh

acx_fn_getEnv

export CVMFSEXEC_HOME="$acx_prefix/\$acx_arch/centos\$acx_os"
. $acx_prefix/mountpoints.sh

if [ ! -e \$CVMFSEXEC_HOME/acxInstalledVersion ]; then
    \echo "Error: cvmfsexec is not installed on \$CVMFSEXEC_HOME"
    return 1
fi

for acx_mount in \${mountPointAr[@]}; do
    acx_fn_printMsg \$acx_time0 "fusermount -u $acx_mount ..."
    fusermount -u \$CVMFSEXEC_HOME/dist/cvmfs/$acx_mount >& /dev/null 
done				

acx_fn_printMsg \$acx_time0 "\rm -rf \$CVMFSEXEC_HOME ..."
\rm -rf \$CVMFSEXEC_HOME >& /dev/null

acx_fn_printMsg \$acx_time0 "force-cleanup completed"

EOF
	let acx_rc=$?
	if [ $acx_rc -eq 0 ]; then
	    \mv $acx_prefix/force-cleanup.sh.tmp  $acx_prefix/force-cleanup.sh
	fi
    fi

    if [[ "$acx_forceInstall" = "YES" ]] || [[ ! -e $acx_prefix/mount.sh ]]; then
	\cat <<EOF >> $acx_prefix/mount.sh.tmp    

let acx_time0=\`date +%s\`

acx_selectedMount=""
if [ \$# -ge 1 ]; then
    acx_selectedMount=",config-osg,\$1,"
fi

. $ACX_BASE/common.sh

acx_fn_printMsg \$acx_time0 "Start mounting ..."

acx_fn_getEnv

export CVMFSEXEC_HOME="$acx_prefix/\$acx_arch/centos\$acx_os"
. $acx_prefix/mountpoints.sh

if [ ! -e \$CVMFSEXEC_HOME/acxInstalledVersion ]; then
    \echo "Error: cvmfsexec is not installed on \$CVMFSEXEC_HOME"
    return 1
fi

for acx_mount in \${mountPointAr[@]}; do
    if [ "\$acx_selectedMount" != "" ]; then
       acx_tmpVal=",\`\echo \$acx_mount | \cut -f 1 -d '.'\`,"
       \echo \$acx_tmpVal | \grep -e "\$acx_tmpVal" > /dev/null 2>&1 
       if [ \$? -eq 0 ]; then
         \echo "Skipping mounting \$acx_mount"
         continue
       fi     
    fi  
    acx_fn_printMsg \$acx_time0 "Mounting $acx_mount ..."
    \$CVMFSEXEC_HOME/mountrepo \$CVMFSEXEC_MOUNTOPTS \$acx_mount
done

export ATLAS_SW_BASE=\$CVMFSEXEC_HOME/dist/cvmfs
export VO_ATLAS_SW_DIR=\$ATLAS_SW_BASE/atlas.cern.ch/repo/sw 
if [ -z \$ALRB_localRelocateDir ]; then
  if [ ! -z \$HOME ]; then
    export ALRB_localRelocateDir=\$HOME/myLocalRelocateDir
  else
    export ALRB_localRelocateDir=\`pwd\`/myLocalRelocateDir
  fi
fi
\mkdir -p \$ALRB_localRelocateDir
export ALRB_RELOCATECVMFS="YES"
export ATLAS_LOCAL_ROOT_BASE=\$ATLAS_SW_BASE/atlas.cern.ch/repo/ATLASLocalRootBase

function setupATLAS()
{
    . \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \$@
    return \$?
}

unset acx_selectedMount
acx_fn_printMsg \$acx_time0 "Mounting completed" 
EOF
	let acx_rc=$?
	if [ $acx_rc -eq 0 ]; then
	    \mv $acx_prefix/mount.sh.tmp $acx_prefix/mount.sh
	fi
    fi

    if [[ "$acx_forceInstall" = "YES" ]] || [[ ! -e $acx_prefix/umount.sh ]]; then
	\cat <<EOF >> $acx_prefix/umount.sh.tmp    
let acx_time0=\`date +%s\`

. $ACX_BASE/common.sh

acx_fn_getEnv

export CVMFSEXEC_HOME="$acx_prefix/\$acx_arch/centos\$acx_os"
. $acx_prefix/mountpoints.sh

if [ ! -e \$CVMFSEXEC_HOME/acxInstalledVersion ]; then
    \echo "Error: cvmfsexec is not installed on \$CVMFSEXEC_HOME"
    return 1
fi

for acx_mount in \${mountPointAr[@]}; do
    acx_fn_printMsg \$acx_time0 "Unmounting $acx_mount ..."
    \$CVMFSEXEC_HOME/umountrepo \$acx_mount
done				
acx_fn_printMsg \$acx_time0 "Unmounting completed"
EOF
   	let acx_rc=$?
	if [ $acx_rc -eq 0 ]; then
	    \mv	$acx_prefix/umount.sh.tmp $acx_prefix/umount.sh
	fi
    fi
    
    return $acx_rc
}
