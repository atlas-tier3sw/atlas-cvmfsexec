#!----------------------------------------------------------------------------
#!
#! common.sh
#!
#! Common functions defined
#!
#! Usage: 
#!     . common.sh
#!
#! History:
#!   12Jan23: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

#!---------------------------------------------------------------------------- 
acx_fn_getEnv()
#!---------------------------------------------------------------------------- 
{
    acx_arch=`uname -m`
    let acx_os=7
    local acx_tmpGLV=`getconf  GNU_LIBC_VERSION 2>&1`
    if [ $? -eq 0 ]; then
	local acx_glv="`\echo $acx_tmpGLV | \awk '{print $NF}' | \awk -F. '{printf "%d%02d", $1, $2}'`"
	
	if [ $acx_glv -le 205 ] ; then
	    let acx_os=5	
	elif [ $acx_glv -le 216 ] ; then
	    let acx_os=6
	elif [ $acx_glv -le 227 ]; then
	    let acx_os=7
	elif [ $acx_glv -le 233 ]; then
	    let acx_os=8
	else
	    let acx_os=9
	fi
    fi

    return 0

}


#!---------------------------------------------------------------------------- 
acx_fn_printMsg()
#!---------------------------------------------------------------------------- 
{
   
    printf "%-s | %s \n" "`date -u '+%F %T'` (ACX: `expr \`date +%s\` - $1` s)" "$2"
    
    return 0
}
