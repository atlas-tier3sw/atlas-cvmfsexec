# atlas-tier3-cvmfsexec

This is a wrapper to install, mount and unmount cvmfsexec in the ATLAS context.

cvmfsexec is described here:
https://github.com/cvmfs/cvmfsexec

For ATLAS, cvmfsexec is built to be made available at the host level so that, for example, pilot wrappers can use it with the simple mount command below.   This also decouples it from container runtime versions that can be independently updated.  setupATLAS -c <container> will mount the relocated cvmfs transparently.

## Installation

```
git clone https://gitlab.cern.ch/atlas-tier3sw/atlas-cvmfsexec.git
cd atlas-cvmfsexec
./install -p <installHome preferably on local disk, not network disk>
```

will install and build cvmfsexec for the platform and os version.
./install -h will list the available options.

## Mount and use

This will be ATLAS ready; it will mount only the ATLAS needed cvmfs repos and
also define the relocated cvmfs repo so that ALRB and tools will work.

Note that to run containers, either user namespeace need to be enabled or a local version of apptainer needs to be installed.

```
source <installHome>/mount.sh
```

You can pass cvmfsexedc options with the env variable priot to the above mount.sh command
```
export CVMFSEXEC_MOUNTOPTS=<options from mountrepo  --help>
```

You can also specify which repo to mount instead of all of atlas;
eg at a minimum
```
source <installHome>/mount.sh atlas,atlas-condb,unpacked
```

After this, simply type
```
setupATLAS
```
or
```
setupATLAS -c <container>
```
and everything should work.  Note that this will define ATLAS_SW_BASE and VO_ATLAS_SW_DIR to point to the relocated cvmfs dir.

## unmount

There will be processes running in the background so you should unmount if you no longer need it.

```
source <installHome>/umount.sh
```



